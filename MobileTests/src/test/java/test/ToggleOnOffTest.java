package test;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class ToggleOnOffTest {

    private AndroidDriver<AndroidElement> driver = null;

    @BeforeClass
    public void setUp() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", "Pixel 2");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "8.1");
        capabilities.setCapability("appPackage", "com.android.settings");
        capabilities.setCapability("appActivity", "com.android.settings.Settings");
        capabilities.setCapability("udid", "emulator-5554");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    }

    @Test
    public void toggleOnOffTest() {
        selectFromList("Display").click();

        waitForElementPresentBy(By.xpath("//android.widget.Switch[contains(@resource-id,'switch_widget')]"));

        List<AndroidElement> elements = driver.findElements(By.xpath("//android.widget.Switch[contains(@resource-id,'switch_widget')]"));

        elements.get(0).click();
        elements.get(0).click();
        elements.get(0).click();
    }

    // Ожидание появления элемента
    public WebElement waitForElementPresentByXpath(By by, String error_message, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.withMessage(error_message+"\n");
        return wait.until(
                ExpectedConditions.presenceOfElementLocated(by)
        );
    }

    //Перегрузка
    public WebElement waitForElementPresentBy(By by) {
        return waitForElementPresentByXpath(by, "Cannot found element", 10);
    }

    private WebElement selectFromList(String text) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='" + text + "']"));
    }

}
