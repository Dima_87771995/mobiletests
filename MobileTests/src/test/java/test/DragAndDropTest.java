package test;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.offset.PointOption.point;

public class DragAndDropTest {

    private AndroidDriver<AndroidElement> driver = null;

    @BeforeClass
    public void setUp() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", "Pixel 2");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "8.1");
        capabilities.setCapability("app", "C:\\mobile\\mobiletests\\MobileTests\\apk\\ApiDemos-debug.apk");
        capabilities.setCapability("udid", "emulator-5554");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    }

    @Test
    public void dragAndDropTest() {
        driver.findElementByXPath("//android.widget.TextView[@text='Views']").click();

        waitForElementPresentBy(By.xpath("//android.widget.TextView[@text='Drag and Drop']")).click();

        MobileElement dragDot1 = driver.findElementById("drag_dot_1");
        MobileElement dragDot2 = driver.findElementById("drag_dot_2");

        Point center1 = dragDot1.getCenter();
        Point center2 = dragDot2.getCenter();

        TouchAction dragAndDrop = new TouchAction(driver);

        dragAndDrop
                .longPress(longPressOptions()
                .withPosition(point(center1.x, center1.y))
                .withDuration(Duration.ofSeconds(2))).moveTo(point(center2.x, center2.y))
                .release()
                .perform();

    }

    // Ожидание появления элемента
    public WebElement waitForElementPresentByXpath(By by, String error_message, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.withMessage(error_message+"\n");
        return wait.until(
                ExpectedConditions.presenceOfElementLocated(by)
        );
    }

    //Перегрузка
    public WebElement waitForElementPresentBy(By by) {
        return waitForElementPresentByXpath(by, "Cannot found element", 10);
    }
}
