package wikipedia;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.LongPressOptions;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofSeconds;

public class WikiTest {

    private AppiumDriver driver;

    @BeforeClass
    public void setUp() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", "Pixel 2");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "8.1");
        capabilities.setCapability("appPackage", "org.wikipedia");
        capabilities.setCapability("appActivity", ".main.MainActivity");
        capabilities.setCapability("app", "C:\\Qa_tutor\\mobile\\apk\\wiki.apk");
        capabilities.setCapability("udid", "emulator-5554");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    }

    @AfterMethod
    public void teardown() {
        driver.quit();
    }

    @Test
    public void wikiTest() {

        WebElement skipBut = driver.findElementByXPath("//android.widget.Button[contains(@text,'SKIP')]");
        skipBut.click();

        WebElement searchElement = waitForElementPresentByXpath("//android.widget.ImageView[contains(@content-desc,'Search Wikipedia')]", "Cannot found", 5);
        searchElement.click();

        WebElement searchLine = waitForElementPresentByXpath("//android.widget.EditText[contains(@text,'Search Wikipedia')]");
        searchLine.sendKeys("Android");

        waitForElementAndClick("//android.widget.TextView[contains(@resource-id,'page_list_item_title') and @text='Android']");

        waitForElementPresentByXpath("//*[contains(@resource-id,'navigation_drawer')]");

//        swipe(1000);

    }

    // Ожидание появления элемента
    public WebElement waitForElementPresentByXpath(String xpath, String error_message, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.withMessage(error_message+"\n");
        By by = By.xpath(xpath);
        return wait.until(
                ExpectedConditions.presenceOfElementLocated(by)
        );
    }

    //Перегрузка
    public WebElement waitForElementPresentByXpath(String xpath) {
        return waitForElementPresentByXpath(xpath, "Cannot found element", 10);
    }

    public WebElement waitForElementAndClick(String xpath) {
        WebElement element = waitForElementPresentByXpath(xpath);
        element.click();
        return element;
    }

    // swipe
    public void swipe(int time) {
        TouchAction action = new TouchAction(driver);

        Dimension size = driver.manage().window().getSize();

        int x = size.width / 2;
        int start_y = (int) (size.height * 0.8);
        int end_y = (int) (size.height * 0.2);

//        action.press(x, start_y).waitAction().moveTo(x, end_y).release().perform();

//        action.longPress(longPressOptions().withPosition(point(x, start_y).moveTo(point(x, end_y))
//                .release()
//                .perform();
    }

}
