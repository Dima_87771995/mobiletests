import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.URL;

public class CalculatorTest {

    private AndroidDriver driver;

    public Elements elements;

    @BeforeClass
    public void setUp() throws Exception{
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "AndroidDevice");
        capabilities.setCapability("platformVersion", "8.1");
        capabilities.setCapability("appPackage", "com.android.calculator2");
        capabilities.setCapability("appActivity", "Calculator");
        capabilities.setCapability("udid", "emulator-5554");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);

        Elements elements = new Elements(driver);
        this.elements = elements;
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void test1() {
        elements.digitSeven().click();
//        driver.findElementById("com.android.calculator2:id/digit_7").click();

        driver.findElementById("op_add").click();

        driver.findElementByXPath("//*[@text='6']").click();

        MobileElement results = (MobileElement) driver.findElementById("com.android.calculator2:id/result");

        Assert.assertEquals("12", results.getText(), "Результат должен быть равен 13");

        System.out.println();
    }

}
