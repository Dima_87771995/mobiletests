package test_from_max;

import from_max.steps.CalculatorSteps;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.URL;

public class MathOperationsTest extends CalculatorSteps {

    private AndroidDriver driver;

    @BeforeClass
    public void setUp() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "AndroidDevice");
        capabilities.setCapability("platformVersion", "8.0");
        capabilities.setCapability("appPackage", "com.android.calculator2");
        capabilities.setCapability("appActivity", "Calculator");
        capabilities.setCapability("udid", "emulator-5554");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @Test(description = "Операция деления")
    public void testDiv() {
        chooseElementById(driver,"op_div", "digit_5", "digit_2" );
        Assert.assertEquals("2.5", getResults(driver).getText(), "Результат должен быть равен 2.5");
        clearElements(driver);
    }

    @Test(description = "Операция умножения")
    public void testMul() {
        chooseElementById(driver,"op_mul", "digit_7", "digit_3" );
        Assert.assertEquals("21", getResults(driver).getText(), "Результат должен быть равен 21");
        clearElements(driver);
    }

    @Test(description = "Операция вычитания")
    public void testMinus() {
        chooseElementById(driver,"op_sub", "digit_9", "digit_1" );
        Assert.assertEquals("8", getResults(driver).getText(), "Результат должен быть равен 8");
        clearElements(driver);
    }

    @Test(description = "Операция сложения")
    public void testPlus() {
        chooseElementById(driver,"op_add", "digit_4", "digit_2" );
        Assert.assertEquals("6", getResults(driver).getText(), "Результат должен быть равен 6");
        clearElements(driver);
    }


}
