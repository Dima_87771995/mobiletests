package from_max.steps;

import from_max.elements.CalculatorElem;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.Step;

public class CalculatorSteps extends CalculatorElem {

    @Step("выбираем кнопку - ОР кнопка выбрана")
    public void chooseElementById(AndroidDriver driver, String operator, String elementOne, String elementTwo ){
        driver.findElementById(elementOne).click();
        driver.findElementById(operator).click();
        driver.findElementById(elementTwo).click();
        driver.findElementById(equals).click();

    }

    @Step("стираем результат - ОР результат стерт")
    public void clearElements(AndroidDriver driver){
        driver.findElementById(clear).click();
    }

    @Step("получаем результат - ОР результат получен")
    public MobileElement getResults (AndroidDriver driver) {
        return  (MobileElement) driver.findElementById(result);
    }


}