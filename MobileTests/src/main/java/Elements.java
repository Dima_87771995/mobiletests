import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;

public class Elements {

    public AndroidDriver driver;

    public Elements(AndroidDriver driver) {
        this.driver = driver;
    }

    public WebElement digitSeven() {
        return driver.findElementById("com.android.calculator2:id/digit_7");
    }
}
